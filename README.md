# click_animation #

v1.0.2

This module enables to change img sources when they're clicked.

## Usage ##

### HTML Classes ###

Just add the class "buttonImg" to your img tag to make the magic happen !

** Example **

```html
<img src="image.png" class"buttonImg" />
```

When clicked, this image src will change to `src="image_clique.png"`.

### Javascript Controls ###

Use Javascript to add this behaviour to buttons you generated bt DOM manipulation.

** Parameters **

```js
ClickAnimation.addClickAnimationEvent(element1, element2)
```

| Parameter 	| Type 		| Default  	| Description 									|
| --------------| --------- | ----------| --------------------------------------------- |
| element1*		| DOM Img 	| 			| The image which src has to change				|
| element2		| DOM * 	| element1	| The control that implies that state change	|