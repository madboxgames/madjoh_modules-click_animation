var dependencies = {
	madjoh_modules : [
		'controls'
	]
};
var list = MadJohRequire.getList(dependencies);
define(list, function(require, Controls){
	var ClickAnimation = {
		addClickAnimationEvent : function(element1, element2){
			new ClickAnimation.clickAnimationListener(element1, element2);
		},
		clickAnimationListener : function(element1, element2){
			if(typeof element1 === 'undefined'){
				console.log('Tried adding a click animation listener to nothing !');
				return;
			}else if(element1.tagName.toLowerCase() !=== 'img'){
				console.log('Tried adding a click animation listener to something that is not an image !');
				return;
			}

			var that = {};

			if(!element2) element2=element1;
			that.element1 = element1;
			that.element2 = element2;

			that.pressedState = function(){
				var path = that.element2.src;
				var newPath = path.substr(0,path.length-4)+'_clique.png';
				that.element2.src = newPath;
			};
			that.releasedState = function(){
				var path = that.element2.src;
				var newPath = path.substr(0,path.length-11)+'.png';
				that.element2.src = newPath;
			};

			that.element1.addEventListener(Controls.pressed, that.pressedState, false);
			that.element1.addEventListener(Controls.cursorEnter, function(){if(Controls.isPressed){that.pressedState();}}, false);

			that.element1.addEventListener(Controls.released, that.releasedState, false);
			that.element1.addEventListener(Controls.cursorLeave, function(){if(Controls.isPressed){that.releasedState();}}, false);

			return that;
		}
	};

	// LISTENERS
		element = document.querySelectorAll('.buttonImg');
		for(var i=0; i<element.length; i++) ClickAnimation.addClickAnimationEvent(element[i], element[i]);

	return ClickAnimation;
});